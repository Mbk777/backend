import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;

public abstract class DBConnection {
	//Attributes
	protected Connection conn = null;
	protected Statement stmt = null;
	
	
	//Constructor
	 DBConnection(){
		try {
			//connection
			this.conn= DriverManager.getConnection("jdbc:mysql://localhost/wedeliver?user=root&password=");
			
			//statement
			this.stmt = conn.createStatement();
	
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	//Crud methods
	 
	
	public abstract <T> List<T> ShowAll() ;
	
	public abstract int Create(String newitems);
	
	public abstract boolean Delete(int ID);
	
	public abstract boolean Update(int ID , String name) ;
}
