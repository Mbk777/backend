import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Category extends DBConnection {

	private int IdCategory;
	private String name;

	public int getIdCategory() {
		return IdCategory;
	}

	public void setIdCategory(int idCategory) {
		IdCategory = idCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> ShowAll() {
		try {
			//SQL
			String sql= "SELECT * from categories";

			//EXECUTE QUERY
			ResultSet rs= this.stmt.executeQuery(sql);

			ArrayList<Category> categories = new ArrayList<Category>();

			while (rs.next()){
				Category	item = new Category();
				item.setIdCategory(rs.getInt("IdCategory"));
				item.setName(rs.getString("Name"));

				categories.add(item);
			}
			return categories;

		}catch(Exception ex) {
			System.out.println("error on ShowAll" + ex.getMessage());
			return null;
		}
	}

	@Override
	public int Create(String newitems) {
		int newid=0;

		try {
			String sql="Insert INTO categories (name) VALUES ('" + newitems + "')";

			int roweffected = this.stmt.executeUpdate(sql);

			if( roweffected > 0) {
				sql = "SELECT MAX(idCategory) as id From categories";

				ResultSet rs = this.stmt.executeQuery(sql);

				if(rs.next()) {
					newid= rs.getInt("id");
				}else {
					throw new Exception("Error to insert data");
				}

			}
			return newid;
		}catch(Exception ex){
			System.out.println("Error on Create" + ex.getMessage());
			return 0;

		}
	}

	@Override
	public boolean Delete(int ID) {
		try {
			String sql= "DELETE from categories where idCategory=" + ID;
			int roweffected = this.stmt.executeUpdate(sql);

			if (roweffected>0) {
				return true;
			}else {
				throw new Exception ("Error to delete data");
			}

		}catch (Exception ex) {
			System.out.println("error on Delete" +ex.getMessage());
			return false;
		}
	}

	@Override
	public boolean Update(int ID, String name) {
		try {
			String sql = "UPDATE categories SET name='" + name + "' where idCategory ='"+ ID + "'";
			int roweffected = this.stmt.executeUpdate(sql);

			if(roweffected>0) {
				return true;
			}else {
				throw new Exception ("Error to Update data");
			} 

		}catch(Exception ex) {
			System.out.println("Error on Updating " + ex.getMessage());
			return false;
		}
	}





}
