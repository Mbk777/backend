import java.util.List;

public class Main {

	public static void main(String[] args) {
		DBConnection test= new Category();
		
		//Create
		int newID = test.Create("TEST");
		
		//UPDATE
		if(test.Update(newID, "TEST2")) {
			System.out.println("Element " + newID + " has been updated ");
		}
		
		//Delete
//		if(test.Delete(newID)) {
//			System.out.println("Element " + newID + " has been deleted");
//		};
		
		
		
		
		//Read
		List<Category> items = test.ShowAll();
		
		for (Category item:items) {
			System.out.println(item.getName());
		}

	}

}